package main

import (
	"net/http"
	"github.com/gorilla/mux"
	"io/ioutil"
	"fmt"
	"encoding/json"
)

func main() {
	http.HandleFunc("/", rpcHandler)
	http.ListenAndServe(":8081", NewRouter())
}

func NewRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/", rpcHandler).Methods("POST")
	return r
}

type Operation struct {
	OperationId string
}

type Result struct {
	Operations []Operation
}

type RPCRequest struct {
	Method string
	Params []interface{}
}

func rpcHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var rpcRequest RPCRequest
	json.Unmarshal(body, &rpcRequest)

	fmt.Printf("Body: %s", string(body))

	result := struct {
		Result []Operation
		Error  error
	}{
		[]Operation{{OperationId: "1234"}},
		nil,
	}

	s, _ := json.Marshal(result)
	w.Header().Set("Content-Type", "application/json")
	w.Write(s)
}
